import { Injectable } from '@angular/core';

@Injectable()
export class CommonService {

  public books: Book[] = [
    new Book(1, 'Android book1', 23.24, 4, 'a book for android', [{ title: 'IT', checked: false }, { title: '金融', checked: true }, { title: '互联网', checked: true }]),
    new Book(2, 'Java book2', 25.56, 3, 'a book for Java', [{ title: 'IT', checked: false }, { title: '金融', checked: true }, { title: '互联网', checked: false }]),
    new Book(3, 'Typescript book3', 30.78, 4, 'a book for Typescript', [{ title: 'IT', checked: false }, { title: '金融', checked: true }, { title: '互联网', checked: false }]),
    new Book(4, 'C# book4', 43.50, 1, 'a book for C#', [{ title: 'IT', checked: true }, { title: '金融', checked: false }, { title: '互联网', checked: true }]),
    new Book(5, 'C++ book5', 12.43, 2, 'a book for C++', [{ title: 'IT', checked: false }, { title: '金融', checked: true }, { title: '互联网', checked: true }]),
    new Book(6, 'ASP.NET book6', 65.33, 5, 'a book for ASP.NET', [{ title: 'IT', checked: false }, { title: '金融', checked: false }, { title: '互联网', checked: true }]),
    new Book(7, 'Testing book7', 43.23, 2, 'a book for Testing', [{ title: 'IT', checked: true }, { title: '金融', checked: true }, { title: '互联网', checked: false }]),
    new Book(8, 'Javascript book8', 20.99, 3, 'a book for Javascript', [{ title: 'IT', checked: false }, { title: '金融', checked: false }, { title: '互联网', checked: true }]),
  ]

  constructor() { }

  getBook() {
    return this.books;
  }

  updateBook(newBook:Book) {
    let id = newBook.id-1;
    this.books[id].name=newBook.name;
    this.books[id].price=newBook.price;
    this.books[id].desc=newBook.desc;
    this.books[id].rating = newBook.rating;
    this.books[id].category[0].checked = newBook.category[0].checked;
    this.books[id].category[1].checked = newBook.category[1].checked;
    this.books[id].category[2].checked = newBook.category[2].checked;
  }

}

export class Book {

  constructor(
    public id: number,
    public name: string,
    public price: number,
    public rating: number,
    public desc: string,
    public category: {}) {
  }
}
