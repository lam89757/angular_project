import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';

import { FormControl } from '@angular/forms';
import{debounceTime} from 'rxjs/operators';

import{Book} from '../../services/common.service';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {

  public searchInput:FormControl=new FormControl();

  public books:any[]=[];
  constructor(public common:CommonService) { }

  ngOnInit() {
    this.books=this.common.getBook();//赋初值
    this.searchInput.valueChanges//搜索栏输入后修改值
    .pipe(debounceTime(500))
    .subscribe((value)=>{
      this.books=this.common.getBook();
      for(let i=0;i<this.books.length;i++){
        if((this.books[i].name).toLowerCase().indexOf(value.toLowerCase())==-1){//根据书名检索输入的值，不区分大小写
          this.books.splice(i--,1)//删除后数组数组长度相应变化，后面的元素会向前移动
        }
      }
      })
    }
  
}
