import { Component, OnInit, OnChanges } from '@angular/core';
import{ActivatedRoute}from '@angular/router';
import { CommonService } from 'src/app/services/common.service';
import {Book} from '../../services/common.service';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';


@Component({
  selector: 'app-bookdetail',
  templateUrl: './bookdetail.component.html',
  styleUrls: ['./bookdetail.component.css']
})
export class BookdetailComponent implements OnInit {

  public book:Book;//Book类实例化对象 
  public bookId:number;//路由传过来的id
  public nameCheck:boolean;//bookName输入校验结果
  public priceCheck:boolean;//bookPrice输入校验结果
  public valueCheck:boolean;//分类对应的checkbox至少选一个的校验结果

  bookModule:FormGroup;//form实例化对象
  fb:FormBuilder=new FormBuilder();

  constructor(public route:ActivatedRoute,public common:CommonService) {
    //初始化book对象，根据传过来的id获取book对象
    this.route.params.subscribe((data)=>{
      this.getBookById(data.id);
      this.bookId=data.id;
    });
    //根据book对象初始化表单，并赋予初始值
    this.bookModule=this.fb.group({
      bookName:[this.book.name,[Validators.required,Validators.minLength(3)]],
      bookPrice:[this.book.price,[Validators.required]],
      bookDesc:[this.book.desc],
      bookCategory:this.fb.array([this.book.category[0].checked,this.book.category[1].checked,this.book.category[2].checked],this.categoryValidator)
    })
  }

  ngOnInit() {
    
  }

  //自定义checkbox校验器，判断3个option值全为false则返回{result:true}
  categoryValidator(bookCategory:FormArray){
    let valid:boolean=false;
    bookCategory.controls.forEach((category)=>{
      if(category.value==true){
        valid=true;
      }
    })
    return valid?null:{result:true};
  }

  //根据路由传过来的id获取book对象
  getBookById(id){
    if(id==0){
      this.book=new Book(0,'',null,0,'',[{ title: 'IT', checked: false }, { title: '金融', checked: false }, { title: '互联网', checked: false }]);
    }else{
      this.book=this.common.getBook()[id-1];
    }
  }

  //子组件传过来值先保存到变量book中，不对数据库做更新，当点击保存按钮在更新数据库
  getRating(rating:number){
    this.book.rating=rating;
    // this.common.updateBook(this.bookId,rating);
  }

  //点击提交按钮，保存修改及新增数据
  updateBook(){
    this.book.name=this.bookModule.value.bookName;
    this.book.price=this.bookModule.value.bookPrice;
    this.book.desc=this.bookModule.value.bookDesc;
    this.book.category[1].checked=this.bookModule.value.bookCategory[1];
    this.book.category[2].checked=this.bookModule.value.bookCategory[2];
    this.book.category[0].checked=this.bookModule.value.bookCategory[0];
    //调用service中的更新图书方法，将修改后的book对象传递到服务器
    this.common.updateBook(this.book);
  }

  //获取bookName表单对应formControl，并传递到HTML中
  get bookName(){return this.bookModule.get('bookName');}

  //检测HTML状态变化，校验各个表单元素
  ngDoCheck(): void {
    this.nameCheck=this.bookModule.get('bookName').valid || this.bookModule.get('bookName').untouched;
    this.priceCheck=this.bookModule.get('bookPrice').valid || this.bookModule.get('bookPrice').untouched;
    this.valueCheck=this.bookModule.get('bookCategory').valid || this.bookModule.get('bookCategory').pristine;
  }

}
